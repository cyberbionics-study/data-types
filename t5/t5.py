import csv
from t3.t3 import MyDialect
from t4.t4 import fieldnames


def average_weight(file):
    """this is function that can count average weight of material in file"""
    with open(file, 'r') as data:
        reader = csv.DictReader(data, fieldnames=fieldnames, dialect=MyDialect)
        count = 0
        summ = 0
        for row in reader:
            count += 1
            if count > 1:
                summ += int(row['weight'])
        res = summ / (count - 1)
        return int(res)


if __name__ == '__main__':
    print(average_weight('materials.csv'))
