import csv
from t3.t3 import MyDialect


csvfile = 'materials.csv'
data = [
    {'id': 1, 'weight': 10, 'height': 5, 'additional characteristics': [('color', 'red'), ('durability', 'high')]},
    {'id': 2, 'weight': 15, 'height': 8, 'additional characteristics': [('color', 'blue'), ('durability', 'mid')]},
    {'id': 3, 'weight': 20, 'height': 10, 'additional characteristics': [('color', 'green'), ('durability', 'low')]},
]

"""here we create cvs file with this data for later work"""
with open(csvfile, 'w') as file:
    fieldnames = ['id', 'weight', 'height', 'additional characteristics']
    writer = csv.DictWriter(file, fieldnames=fieldnames, dialect=MyDialect)
    writer.writeheader()
    for row in data:
        writer.writerow(rowdict=row)

with open(csvfile, 'r') as file:
    print(file.read())
