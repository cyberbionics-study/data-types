from faker import Faker
from xml.etree import ElementTree as ETr


fake = Faker()
personal_data = ETr.Element('data')

"""creating xml file with fake personal data"""
for _ in range(20):
    record = ETr.SubElement(personal_data, 'record')
    name = ETr.SubElement(record, 'name')
    name.text = str(fake.name())
    address = ETr.SubElement(record, 'address')
    address.text = str(fake.address())
    phones = ETr.SubElement(record, 'phones')
    phone = ETr.SubElement(phones, 'phone')
    phone.text = str(fake.phone_number())
    emails = ETr.SubElement(record, 'emails')
    email = ETr.SubElement(emails, 'email')
    email.text = str(fake.ascii_email())

tree = ETr.ElementTree(personal_data)
tree.write('data.xml', encoding='utf-8')
