"""here we try to do some requests"""
import xml.etree.ElementTree as ET

tree = ET.parse('data.xml')
root = tree.getroot()

names = root.findall(".//name")

for name in names:
    print(name.text)

print('')
print('')

"""i don't get why this is not working. i tried many forms of request, but always get something like this:
...
KeyError: (".//name[starts-with('H')]",)

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "C:\Users\38098\Desktop\учеба\CyberBionics\homework\моё\python advanced\data-types\t2\t2_search.py", line 14, in <module>
    some_records = root.findall(".//name[starts-with('H')]")
                   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "C:\Program Files\Python311\Lib\xml\etree\ElementPath.py", line 411, in findall
    return list(iterfind(elem, path, namespaces))
                ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "C:\Program Files\Python311\Lib\xml\etree\ElementPath.py", line 384, in iterfind
    selector.append(ops[token[0]](next, token))
                    ^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "C:\Program Files\Python311\Lib\xml\etree\ElementPath.py", line 337, in prepare_predicate
    raise SyntaxError("invalid predicate")
SyntaxError: invalid predicate
"""
# some_records = root.findall(".//name[starts-with('H')]")
#
# for record in some_records:
#     name = record.find("./name").text
#     address = record.find("./address").text
#     print(f"Name: {name}, Address: {address}")
