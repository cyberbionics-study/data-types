import json


d1 = {}
d2 = {}
d3 = {}


"""creating 3 simple dicts"""
for a in 'abc':
    d1[a] = a.upper()

for a in 'def':
    d2[a] = a.upper()

for a in 'hij':
    d3[a] = a.upper()

with open('data.json', 'w+') as data:
    """writing data in json like 3 separate objects"""
    json.dump(d1, data)
    data.write('\n')
    json.dump(d2, data)
    data.write('\n')
    json.dump(d3, data)
    data.write('\n')

with open('data.json') as data_read:
    """reading json"""
    for line in data_read:
        data = json.loads(line)
        print(data)
