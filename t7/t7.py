import csv
import json
import xml.etree.ElementTree as ET


def convert_to_json(conv_file):
    """function is converting csv file to json"""
    data = []

    with open(conv_file, 'r') as csvfile:
        csv_reader = csv.reader(csvfile)
        for row in csv_reader:
            data.append(row)

    with open('data.json', 'w') as jsonfile:
        json.dump(data, jsonfile, indent=4)


def convert_to_xml(conv_file):
    """function is converting csv file to xml"""
    root = ET.Element("data")

    with open(conv_file, 'r') as csvfile:
        csv_reader = csv.DictReader(csvfile)
        for row in csv_reader:
            record = ET.SubElement(root, "record")
            for key, value in row.items():
                field = ET.SubElement(record, key)
                field.text = value

    tree = ET.ElementTree(root)
    tree.write('data.xml')


def operate_with_file():
    """this function when called allows us to operate with csv file in some ways from console:
    write/rewrite
    add info
    read
    convert to json and xml
    also it has some help option"""
    file = 'data.csv'
    print('input "help" or "?" for info')
    while True:
        x = input('please pick option: ')
        if x in ('help', '?'):
            print('available options:\nrewrite - input "w"\nadd data - input "a"\nread file - input "r"\n'
                  'convert to json - input "json"\nconvert to xml - input "xml"')
        elif x == 'w':
            with open(file, 'w', newline='', encoding="utf-8") as data:
                fieldnames = ['name', 'surname', 'birthdate', 'address']
                writer = csv.DictWriter(data, fieldnames=fieldnames, quoting=csv.QUOTE_ALL)
                writer.writeheader()
                writer.writerow({
                    'name': input('input name: '),
                    'surname': input('input surname: '),
                    'birthdate': input('input birthdate: '),
                    'address': input('input address: ')
                })
        elif x == 'a':
            with open(file, 'a+') as data:
                fieldnames = ['name', 'surname', 'birthdate', 'address']
                writer = csv.DictWriter(data, fieldnames=fieldnames, quoting=csv.QUOTE_ALL)
                writer.writerow({
                    'name': input('input name: '),
                    'surname': input('input surname: '),
                    'birthdate': input('input birthdate: '),
                    'address': input('input address: ')
                })
        elif x == 'r':
            with open(file, 'r', newline='', encoding="utf-8") as data:
                persons = csv.reader(data)
                persons_list = [person for person in persons]
                for line in persons_list:
                    print(line)
        elif x == 'json':
            convert_to_json(file)
        elif x == 'xml':
            convert_to_xml(file)
        else:
            print('unknown command')
            break


if __name__ == '__main__':
    operate_with_file()
