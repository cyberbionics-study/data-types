import csv
from t3.t3 import MyDialect


def concatenate_fields(file):
    """this function do concatenation of all fields in file.
    WARNING!!!
    I am absolutely not sure that this function works correctly"""
    result = ''
    with open(file, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile, dialect=MyDialect)
        for row in reader:
            for field in row:
                result += str(field)
    return result


if __name__ == '__main__':
    print(concatenate_fields('materials.csv'))
