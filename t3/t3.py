"""file where some custom csv dialect is described"""
import csv


class MyDialect(csv.Dialect):
    delimiter = ';'
    quotechar = '"'
    doublequote = True
    escapechar = None
    lineterminator = '\n'
    quoting = 1


"""some local test"""
if __name__ == '__main__':

    with open('my_data.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, dialect=MyDialect)
        writer.writerow(['Имя', 'Возраст', 'Город'])
        writer.writerow(['Иван', 30, 'Москва'])
        writer.writerow(['Мария', 25, 'Санкт-Петербург'])

    with open('my_data.csv', 'r', newline='') as csvfile:
        reader = csv.reader(csvfile, dialect=MyDialect)
        next(reader, None)
        for row in reader:
            name, age, city = row
            print(f'Имя: {name}, Возраст: {age}, Город: {city}')
